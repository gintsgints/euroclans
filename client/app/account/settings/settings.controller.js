'use strict';

export default class SettingsController {
  user = {
    oldPassword: '',
    newPassword: '',
    confirmPassword: ''
  };
  errors = {
    other: undefined
  };
  message = '';
  message2 = '';
  submitted = false;
  submitted2 = false;


  /*@ngInject*/
  constructor(Auth) {
    this.Auth = Auth;
  }

  $onInit() {
    this.Auth.getCurrentUser().then(user => {
      this.user = user;
    });
  }

  updateProfile(form) {
    this.submitted2 = true;

    if (form.$valid) {
      this.Auth.changeProfile(this.user)
        .then(() => {
          this.message2 = 'Profile successfully updated.';
        });
    }
  }

  changePassword(form) {
    this.submitted = true;

    if (form.$valid) {
      this.Auth.changePassword(this.user.oldPassword, this.user.newPassword)
        .then(() => {
          this.message = 'Password successfully changed.';
        })
        .catch(() => {
          form.password.$setValidity('mongoose', false);
          this.errors.other = 'Incorrect password';
          this.message = '';
        });
    }
  }
}
