'use strict';
const angular = require('angular');

export class gameComponent {
  /*@ngInject*/
  constructor($http, $stateParams, $state) {
    this.$http = $http;
    this.$stateParams = $stateParams;
    this.$state = $state;
    this.game = {};
    this.game.gameMaps = [];
    this.game.gameTypes = [];
    this.gametypes = [];
    this.gameplatforms = [];
  }

  $onInit() {
    this.thisId = this.$stateParams.id;
    if (this.thisId) {
      this.$http.get('/api/games/'.concat(this.thisId))
        .then(response => {
          this.game = response.data;
        });
    }
    this.$http.get('/api/game_types/')
      .then(response => {
        this.gametypes = response.data;
      });
    this.$http.get('/api/game_platforms/')
      .then(response => {
        this.gameplatforms = response.data;
      });
  }

  addGameType() {
    this.game.gameTypes.push({ name: this.chosengametype });
    this.chosengametype = '';
  }

  removeGameType(gametype) {
    let index = this.game.gameTypes.indexOf(gametype);
    this.game.gameTypes.splice(index, 1);
  }

  addGameMap() {
    this.game.gameMaps.push({ name: this.chosengamemap });
    this.chosengamemap = '';
  }

  removeGameMap(gamemap) {
    let index = this.game.gameMaps.indexOf(gamemap);
    this.game.gameMaps.splice(index, 1);
  }

  saveGame() {
    if (this.game._id) {
      this.$http.put('/api/games/'.concat(this.game._id), this.game)
        .then(response => {
          this.response = response;
          this.$state.go('admin.games');
        });
    } else {
      this.$http.post('/api/games', this.game)
        .then(response => {
          this.response = response;
          this.$state.go('admin.games');
        });
    }
  }
}

export default angular.module('euroclansApp.game', [])
  .component('game', {
    template: require('./game.html'),
    bindings: { message: '<' },
    controller: gameComponent
  })
  .name;
