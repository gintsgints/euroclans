'use strict';

/* globals describe, angular, expect, it, beforeEach, inject */

import game from './game.component';

describe('Component: game', function() {
  // load the component's module
  beforeEach(angular.mock.module(game));
  beforeEach(angular.mock.module('stateMock'));

  var gameComponent;
  var state;

  // Initialize the component and a mock scope
  beforeEach(inject(function(_$httpBackend_, $http, $componentController, $stateParams, $state) {
    state = $state;

    gameComponent = $componentController('game', { $state: state });
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
