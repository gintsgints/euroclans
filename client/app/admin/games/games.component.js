'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './games.routes';

export class GamesComponent {
  /*@ngInject*/
  constructor($http) {
    this.$http = $http;
    this.games = [];
  }

  $onInit() {
    this.$http.get('/api/games')
      .then(response => {
        this.games = response.data;
      });
  }
}

export default angular.module('euroclansApp.games', [uiRouter])
  .config(routes)
  .component('games', {
    template: require('./games.html'),
    controller: GamesComponent
  })
  .name;
