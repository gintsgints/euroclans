'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('admin.games', {
      url: '/games',
      template: '<games></games>'
    })
    .state('admin.games_add', {
      url: '/games/add',
      template: '<game></game>'
    })
    .state('admin.games_edit', {
      url: '/games/:id',
      template: '<game></game>'
    });
}
