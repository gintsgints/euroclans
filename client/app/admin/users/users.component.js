'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './users.routes';

export class UsersComponent {
  /*@ngInject*/
  constructor(User) {
    // Use the User $resource to fetch all users
    this.users = User.query();
  }

  delete(user) {
    user.$remove();
    this.users.splice(this.users.indexOf(user), 1);
  }
}

export default angular.module('euroclansApp.users', [uiRouter])
  .config(routes)
  .component('users', {
    template: require('./users.html'),
    controller: UsersComponent
  })
  .name;
