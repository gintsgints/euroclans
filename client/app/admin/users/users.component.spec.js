'use strict';

/* globals describe, angular, expect, it, beforeEach, inject */

import users from './users.component';

describe('Component: UsersComponent', function() {
  // load the controller's module
  beforeEach(angular.mock.module(users));
  beforeEach(angular.mock.module('userMock'));

  var UsersComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController, User) {
    UsersComponent = $componentController('users', { User });
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
