'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('admin.users', {
      url: '/users',
      template: '<users></users>'
    });
}
