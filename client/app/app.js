'use strict';

import angular from 'angular';
// import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';
import ngAnimate from 'angular-animate';
import ngCalendar from 'angular-bootstrap-calendar';

import 'angular-socket-io';
import 'angularjs-scroll-glue';

import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import 'angular-validation-match';

import {
  routeConfig
} from './app.config';

import _Auth from '../components/auth/auth.module';
import account from './account';
import admin from './admin';
import users from './admin/users/users.component';
import games from './admin/games/games.component';
import game from './admin/games/game/game.component';
import navbar from '../components/navbar/navbar.component';
import player from '../components/player/player.component';
import footer from '../components/footer/footer.component';
import main from './main/main.component';
import event from './event/event.component';
import clan from './clans/clan/clan.component';
import clans from './clans/clans.component';
import constants from './app.constants';
import util from '../components/util/util.module';
import socket from '../components/socket/socket.service';

import './app.scss';

angular.module('euroclansApp', [ngCookies, ngAnimate, ngResource, ngSanitize, 'btford.socket-io', uiRouter, ngCalendar,
    uiBootstrap, _Auth, account, admin, users, games, game, 'validation.match', 'luegg.directives', navbar, footer, player, main, clan, clans, event, constants,
    socket, util
  ])
  .config(routeConfig)
  .run(function($rootScope, $location, Auth) {
    'ngInject';
    // Redirect to login if route requires auth and you're not logged in

    $rootScope.$on('$stateChangeStart', function(vEvent, next) {
      Auth.isLoggedIn(function(loggedIn) {
        if (next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  });

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['euroclansApp'], {
      strictDi: true
    });
  });
