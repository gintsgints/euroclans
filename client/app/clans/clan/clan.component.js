'use strict';
const angular = require('angular');

export class clanComponent {
  /*@ngInject*/
  constructor($http, socket, Auth, $state, $stateParams) {
    this.isLoggedIn = Auth.isLoggedInSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
    this.$http = $http;
    this.$stateParams = $stateParams;
    this.$state = $state;
    this.socket = socket;
    this.thisId = '';
    this.mymsg = '';
    this.editMode = false;
    this.clans = [];
    this.joinrequests = [];
    this.games = [];
    this.users = [];
  }

  $onInit() {
    this.$http.get('/api/games')
      .then(response => {
        this.games = response.data;
      });
    this.$http.get('/api/users/list')
      .then(response => {
        this.users = response.data;
      });
    this.thisId = this.$stateParams.id;
    if (this.thisId) {
      this.$http.get('/api/clans/'.concat(this.thisId))
        .then(response => {
          this.clans[0] = response.data;
          this.socket.syncUpdates('clan', this.clans);
          this.$http.get('/api/join_requests', { params: { clan: this.clans[0]._id } })
            .then(jrresponse => {
              this.joinrequests = jrresponse.data;
              this.socket.syncUpdates('join_request', this.joinrequests);
            });
        });
    } else {
      this.clans.push({ active: true });
    }
  }

  transferClan() {
    if (this.newowner) {
      this.clans[0].owner = this.newowner._id;
      this.$http.put('/api/clans/'.concat(this.clans[0]._id), this.clans[0])
        .then(response => {
          this.response = response;
          this.editMode = false;
        });
    }
  }

  editClan() {
    this.editMode = true;
  }

  saveClan(clan) {
    this.$http.put('/api/clans/'.concat(clan._id), clan)
      .then(response => {
        this.response = response;
        this.editMode = false;
      });
  }

  createClan(clan) {
    this.$http.post('/api/clans', clan)
      .then(response => {
        this.response = response;
        this.$state.go('clans.list');
      });
  }

  kickPlayer(indexOf) {
    this.$http.patch('/api/clans/'.concat(this.clans[0]._id), [{ op: 'remove', path: '/members/' + indexOf }])
      .then(response => {
        this.clans[0] = response.data;
      }, reason => {
        console.log('Error while posting clan update: ', reason);
      });
  }

  joinRequest(clan) {
    this.$http.post('/api/join_requests', { clan, user: this.getCurrentUser()._id })
      .then(response => {
        this.response = response;
        this.joinrequests.push(response.data);
      });
  }

  acceptRequest(joinrequest) {
    this.$http.patch('/api/clans/'.concat(this.clans[0]._id), [{ op: 'add', path: '/members/0', value: { _id: joinrequest.user } }])
      .then(response => {
        this.clans[0] = response.data;
        this.declineRequest(joinrequest);
      }, reason => {
        console.log('Error while posting clan update: ', reason);
      });
  }

  declineRequest(joinrequest) {
    this.$http.delete('/api/join_requests/'.concat(joinrequest._id))
      .then(response => {
        this.response = response;
        this.joinrequests.forEach(request => {
          if (request._id === joinrequest._id) {
            this.joinrequests.splice(this.joinrequests.indexOf(request), 1);
          }
        });
      });
  }

  addMessage() {
    this.$http.patch('/api/clans/'.concat(this.clans[0]._id), [{ op: 'add', path: '/chat/0', value: { user: this.getCurrentUser()._id, message: this.mymsg } }])
      .then(response => {
        this.mymsg = '';
        this.clans[0] = response.data;
      }, reason => {
        console.log('Error while posting event update for chat: ', reason);
      });
  }

  clearChat() {
    this.clans[0].chat = [];
    this.saveClan(this.clans[0]);
  }
}

export default angular.module('euroclansApp.clan', [])
  .component('clan', {
    template: require('./clan.html'),
    bindings: { message: '<' },
    controller: clanComponent
  })
  .name;
