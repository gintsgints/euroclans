'use strict';

/* globals describe, angular, expect, it, beforeEach, inject */

import clan from './clan.component';

describe('Component: clan', function() {
  // load the component's module
  beforeEach(angular.mock.module(clan));
  beforeEach(angular.mock.module('socketMock'));
  beforeEach(angular.mock.module('authMock'));
  beforeEach(angular.mock.module('stateMock'));

  var clanComponent;

  // Initialize the component and a mock scope
  beforeEach(inject(function($componentController, socket, Auth, $state) {
    clanComponent = $componentController('clan', { socket, Auth, $state });
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
