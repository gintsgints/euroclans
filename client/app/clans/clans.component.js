'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './clans.routes';

export class ClansComponent {
  /*@ngInject*/
  constructor($http, Auth, socket, $state) {
    this.isLoggedIn = Auth.isLoggedInSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
    this.$http = $http;
    this.socket = socket;
    this.$state = $state;
    this.clans = [];
  }

  $onInit() {
    this.$http.get('/api/clans')
      .then(response => {
        this.clans = response.data;
        this.socket.syncUpdates('clan', this.clans);
      });
  }

  goDetails(id) {
    this.$state.go('clans.detail', { id });
  }
}

export default angular.module('euroclansApp.clans', [uiRouter])
  .config(routes)
  .component('clans', {
    template: require('./clans.html'),
    controller: ClansComponent
  })
  .name;
