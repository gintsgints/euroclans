'use strict';

/* globals describe, angular, expect, it, beforeEach, inject */

import clans from './clans.component';

describe('Component: ClansComponent', function() {
  // load the controller's module
  beforeEach(angular.mock.module(clans));
  beforeEach(angular.mock.module('stateMock'));
  beforeEach(angular.mock.module('socketMock'));
  beforeEach(angular.mock.module('authMock'));

  var ClansComponent;
  var state;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController, $stateParams, $state, socket, Auth) {
    state = $state;

    ClansComponent = $componentController('clans', { $state: state, socket, Auth });
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
