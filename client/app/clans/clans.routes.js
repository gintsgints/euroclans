'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('clans', {
      url: '/clans',
      abstract: true,
      template: '<ui-view/>'
    })
    .state('clans.list', {
      url: '/',
      template: '<clans></clans>'
    })
    .state('clans.add', {
      url: '/add',
      template: '<clan></clan>'
    })
    .state('clans.edit', {
      url: '/:id',
      template: '<clan></clan>'
    })
    .state('clans.detail', {
      url: '/:id',
      template: '<clan></clan>'
    });
}
