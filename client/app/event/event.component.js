'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './event.routes';

export class EventComponent {
  events = [];

  /*@ngInject*/
  constructor($http, socket, Auth, $state, $stateParams, $filter) {
    this.isLoggedIn = Auth.isLoggedInSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
    this.isInClan = Auth.isInClan;
    this.$http = $http;
    this.$stateParams = $stateParams;
    this.$state = $state;
    this.socket = socket;
    this.$filter = $filter;
    this.thisId = '';
    this.clanEvent = false;
    // if ($state.current.name == 'event.edit') {
    //   this.editMode = true;
    // } else {
    this.editMode = false;
    // }
    this.resultsMode = false;
    this.dateOptions = {
      formatYear: 'yy',
      maxDate: new Date(2020, 5, 22),
      minDate: new Date(),
      startingDay: 1
    };
    this.popup = {
      opened: false
    };
    this.events = [];
    this.games = [];
    this.clans = [];
  }

  $onInit() {
    this.$http.get('/api/games')
      .then(response => {
        this.games = response.data;
      });
    this.$http.get('/api/clans')
      .then(response => {
        this.clans = response.data;
      });
    this.thisId = this.$stateParams.id;
    if (this.thisId) {
      this.$http.get('/api/events/'.concat(this.thisId))
        .then(response => {
          this.events[0] = response.data;
          if (this.events[0].resultLeft) {
            this.resultsMode = true;
            if (this.events[0].confirmedLeft && this.events[0].confirmedRight) {
              this.resultsMode = false;
            }
          }
          this.events[0].datetime = new Date(response.data.datetime);
          this.socket.syncUpdates('event', this.events);
        });
    } else {
      this.events.push({});
    }
  }

  onMapSelect(item) {
    if (!this.events[0].gameMaps) {
      this.events[0].gameMaps = [];
    }
    this.events[0].gameMaps.push(item);
    this.gamemap = '';
  }

  removeGameMap(gameMap) {
    let index = this.events[0].gameMaps.indexOf(gameMap);
    this.events[0].gameMaps.splice(index, 1);
  }

  addMessage() {
    // this.events[0].chat.push({ user: this.getCurrentUser()._id, message: this.mymsg });
    this.$http.patch('/api/events/'.concat(this.events[0]._id), [{ op: 'add', path: '/chat/0', value: { user: this.getCurrentUser()._id, message: this.mymsg } }])
      .then(response => {
        this.mymsg = '';
        this.events[0] = response.data;
      }, reason => {
        console.log('Error while posting event update for chat: ', reason);
      });
  }

  changeClanEvent() {
    this.clanEvent = !this.clanEvent;
    if (!this.clanEvent) {
      this.events[0].leftClan = undefined;
      this.events[0].rightClan = undefined;
    }
  }

  openPopup() {
    this.popup.opened = true;
  }

  readyText(event) {
    if (event) {
      if (event.datetime === undefined) {
        return ' When all players ready';
      } else {
        return ' '.concat(this.$filter('date')(event.datetime, 'yyyy-MM-dd HH:mm'));
      }
    }
  }

  editEvent() {
    this.editMode = true;
  }

  deleteEvent() {
    this.$http.delete('/api/events/'.concat(this.events[0]._id))
      .then(response => {
        this.response = response;
        this.$state.go('main');
      });
  }

  createEvent(event) {
    this.$http.post('/api/events', event)
      .then(response => {
        this.response = response;
        this.$state.go('main');
      });
  }

  saveEvent(event) {
    this.$http.put('/api/events/'.concat(event._id), event)
      .then(response => {
        this.response = response;
        this.editMode = false;
      });
  }

  addResult() {
    this.resultsMode = true;
    this.events[0].resultLeft = 0;
    this.events[0].resultRight = 0;
  }

  // Returns which team player is 0 - none, 1 - left, 2 - right
  whichTeam(_id) {
    var team = 0;
    if (_id && this.events[0]) {
      this.events[0].rightTeam.forEach(id => {
        if (id == _id) {
          team = 2;
        }
      });
      this.events[0].leftTeam.forEach(id => {
        if (id == _id) {
          team = 1;
        }
      });
    }
    return team;
  }

  saveMaps() {
    this.$http.patch('/api/events/'.concat(this.events[0]._id), [{ op: 'replace', path: '/gameMaps', value: this.events[0].gameMaps }])
      .then(response => {
        this.events[0] = response.data;
      }, reason => {
        console.log('Error while posting event update: ', reason);
      });
  }

  confirmEvent(side) {
    if (side === 0) {
      this.events[0].confirmedLeft = true;
    } else {
      this.events[0].confirmedRight = true;
    }
    this.saveEvent(this.events[0]);
  }

  joinEvent(side, event) {
    let patchpath = '';
    if (side === 0) {
      patchpath = '/leftTeam/0';
    } else {
      patchpath = '/rightTeam/0';
    }
    this.$http.patch('/api/events/'.concat(event._id), [{ op: 'add', path: patchpath, value: { _id: this.getCurrentUser()._id } }])
      .then(response => {
        this.events[0] = response.data;
      }, reason => {
        console.log('Error while posting event update: ', reason);
      });
  }

  leaveEvent(side, event) {
    let patchpath = '';
    if (side === 0) {
      patchpath = '/leftTeam/0';
    } else {
      patchpath = '/rightTeam/0';
    }
    this.$http.patch('/api/events/'.concat(event._id), [{ op: 'remove', path: patchpath, value: { _id: this.getCurrentUser()._id } }])
      .then(response => {
        this.events[0] = response.data;
      }, reason => {
        console.log('Error while posting event update: ', reason);
      });
  }

}

export default angular.module('euroclansApp.event', [uiRouter])
  .config(routes)
  .component('event', {
    template: require('./event.html'),
    controller: EventComponent
  })
  .name;
