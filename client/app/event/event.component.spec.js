'use strict';

/* globals describe, angular, expect, it, beforeEach, inject */

import event from './event.component';

describe('Component: EventComponent', function() {
  // load the controller's module
  beforeEach(angular.mock.module(event));
  beforeEach(angular.mock.module('stateMock'));
  beforeEach(angular.mock.module('socketMock'));
  beforeEach(angular.mock.module('authMock'));

  var EventComponent;
  var state;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController, $stateParams, $state, socket, Auth) {
    state = $state;

    EventComponent = $componentController('event', { $state: state, socket, Auth });
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
