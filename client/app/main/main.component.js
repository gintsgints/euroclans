import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './main.routes';
import moment from 'moment';

export class MainController {
  events = [];
  newEvent = '';

  /*@ngInject*/
  constructor($http, $filter, $scope, $locale, $state, socket, Auth, calendarConfig) {
    this.isLoggedIn = Auth.isLoggedInSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
    this.$http = $http;
    this.$filter = $filter;
    this.socket = socket;
    this.$state = $state;

    this.calendarConfig = calendarConfig;
    this.calendarConfig.dateFormatter = 'moment';
    this.calendarView = 'week';
    moment.updateLocale('en_gb', {
      week: {
        dow: 1 // Monday is the first day of the week
      }
    });
    this.viewDate = new Date();
    this.calevents = [];
  }

  checkEventShow() {
    return function(event) {
      return new Date(event.datetime ? event.datetime : new Date()) > moment().startOf('day');
    };
  }

  checkEventNull() {
    return function(event) {
      return event.datetime === undefined;
    };
  }

  checkEventNotNull() {
    return function(event) {
      return event.datetime !== undefined;
    };
  }

  readyText(event) {
    if (event.datetime === undefined) {
      return ' When all players ready';
    } else {
      return ' '.concat(this.$filter('date')(event.datetime, 'yyyy-MM-dd HH:mm'));
    }
  }

  joinEvent(side, event) {
    let patchpath = '';
    if (side === 0) {
      patchpath = '/leftTeam/0';
    } else {
      patchpath = '/rightTeam/0';
    }
    this.$http.patch('/api/events/'.concat(event._id), [{ op: 'add', path: patchpath, value: { _id: this.getCurrentUser()._id } }])
      .then(response => {
        event = response.data;
      }, reason => {
        console.log('Error while posting event update: ', reason);
      });
  }

  $onInit() {
    this.$http.get('/api/events')
      .then(response => {
        this.events = response.data;
        this.calevents = this.events.map(event => ({
          title: `
          <i class="glyphicon glyphicon-${event.finished ? 'check' : 'calendar'}"></i> ${event.name}
          `,
          _id: event._id,
          startsAt: new Date(event.datetime ? event.datetime : new Date()),
          color: { primary: '#e3bc08', secondary: '#1c1e22' },
          actions: [{
            label: '<i class=\'glyphicon glyphicon-pencil\'></i>',
            onClick: args => {
              this.$state.go('event.edit', { id: args.calendarEvent._id });
            }
          }],
        }));
        this.socket.syncUpdates('event', this.events);
      });
  }

  goDetails(id) {
    this.$state.go('event', { id });
  }

  eventClicked(calendarEvent) {
    this.$state.go('event', { id: calendarEvent._id });
  }

  addThing() {
    if (this.newEvent) {
      this.$http.post('/api/events', {
        name: this.newEvent
      });
      this.newThing = '';
    }
  }

}

export default angular.module('euroclansApp.main', [uiRouter])
  .config(routing)
  .component('main', {
    template: require('./main.html'),
    controller: MainController
  })
  .name;
