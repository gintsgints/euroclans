'use strict';

/* globals describe, angular, expect, it, beforeEach, inject */

import main from './main.component';

describe('Component: MainComponent', function() {
  beforeEach(angular.mock.module(main));
  beforeEach(angular.mock.module('stateMock'));
  beforeEach(angular.mock.module('socketMock'));
  beforeEach(angular.mock.module('authMock'));
  beforeEach(angular.mock.module('calendarConfigMock'));

  var scope;
  var mainComponent;
  var state;
  var $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function(_$httpBackend_, $http, $componentController, $rootScope, $state, Auth, socket, calendarConfig) {
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('/api/events')
      .respond([{ name: 'Event1' }]);

    scope = $rootScope.$new();
    state = $state;
    mainComponent = $componentController('main', {
      $http,
      $scope: scope,
      $state: state,
      socket,
      Auth,
      calendarConfig
    });
  }));

  it('should attach a list of things to the controller', function() {
    mainComponent.$onInit();
    $httpBackend.flush();
    expect(mainComponent.calevents.length)
      .to.equal(1);
  });
});
