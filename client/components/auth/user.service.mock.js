'use strict';

const angular = require('angular');

angular.module('userMock', []);
angular.module('userMock')
  .service('User', function() {
    this.query = function() {
      return [{ name: 'user1' }, { name: 'user2' }];
    };
  });
