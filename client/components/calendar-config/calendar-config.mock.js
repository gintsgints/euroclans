'use strict';

const angular = require('angular');

angular.module('calendarConfigMock', [])
  .factory('calendarConfig', function() {
    return {
      calendarConfig: {}
    };
  });
