import angular from 'angular';

export class PlayerController {
  playerData = {};

  /*@ngInject*/
  constructor($http) {
    this.$http = $http;
  }

  $onChanges() {
    this.$http.get('/api/users/'.concat(this.player))
      .then(response => {
        this.playerData = response.data;
      });
  }

}

export default angular.module('directives.player', [])
  .component('player', {
    template: require('./player.html'),
    bindings: { player: '<' },
    controller: PlayerController
  })
  .name;
