'use strict';

import angular from 'angular';

export default angular
  .module('euroclansApp.scrollNav', [])
  .directive('scrollNav', function($window) {
    'ngInject';
    return function(scope) {
      angular.element($window)
        .bind('scroll', function() {
          if (!scope.scrollPosition) {
            scope.scrollPosition = 70;
          }
          if (!scope.innerWidth) {
            scope.innerWidth = this.innerWidth;
          }
          if ((this.pageYOffset > scope.scrollPosition) && scope.innerWidth > 750) {
            scope.boolChangeClass = true;
          } else {
            scope.boolChangeClass = false;
          }
          scope.$apply();
        })
        .bind('resize', function() {
          scope.innerWidth = $window.innerWidth;
          if (!scope.scrollPosition) {
            scope.scrollPosition = 70;
          }
          if ((this.pageYOffset > scope.scrollPosition) && scope.innerWidth > 750) {
            scope.boolChangeClass = true;
          } else {
            scope.boolChangeClass = false;
          }
          scope.$apply();
        });
    };
  }).name;
