'use strict';

/* globals element by */

var EventPage = function() {
  var form = this.form = element(by.css('body'));
  form.name = form.element(by.model('$ctrl.events[0].name'));
  form.game = form.element(by.model('$ctrl.events[0].game'));
};

module.exports = new EventPage();
