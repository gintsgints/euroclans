'use strict';

/* globals describe it by protractor require browser before expect */

var config = browser.params;
var UserModel = require(config.serverConfig.root + '/server/api/user/user.model').default;
var GameModel = require(config.serverConfig.root + '/server/api/game/game.model').default;

describe('Event form', function() {
  var page;

  var login = function(user) {
    let promise = browser.get(config.baseUrl + '/login');
    require('../account/login/login.po').login(user);
    return promise;
  };

  var loadPage = function() {
    let promise = browser.get(config.baseUrl.concat('/event/'));
    page = require('./event.po');
    return promise;
  };

  var testUser = {
    name: 'Test User',
    email: 'polis.gints@gmail.com',
    password: 'test'
  };

  var testGame = {
    name: 'Battlefield 1',
    gameTypes: [],
    gamePlatforms: [],
    img: 'bf1.jpg',
    active: true
  };

  before(function() {
    return UserModel
      .remove()
      .then(function() {
        return UserModel.create(testUser);
      })
      .then(function() {
        return GameModel.create(testGame);
      })
      .then(() => {
        login(testUser);
      })
      .then(loadPage);
  });

  it('should have all inputs', function() {
    expect(page.form.name);
    expect(page.form.game);

    expect(page.form.name.getAttribute('type')).to.eventually.equal('text');
    expect(page.form.name.getAttribute('name')).to.eventually.equal('name');
    expect(page.form.game.getAttribute('type')).to.eventually.equal('text');
    expect(page.form.game.getAttribute('name')).to.eventually.equal('game');
  });

  // TODO: fix test
  // it('should show game list', function() {
  //   page.form.game.click().then(() => {
  //     page.form.game.sendKeys('Batt');
  //     browser.pause();
  //     page.form.game.sendKeys(protractor.Key.TAB);
  //     // page.form.game.element(by.css('a')).getAttribute('title').to.eventually.equal('Battlefield 1');
  //   });
  // });
});
