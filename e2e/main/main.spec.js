'use strict';

var config = browser.params;

describe('Main View', function() {
  var page;

  beforeEach(function() {
    let promise = browser.get(config.baseUrl + '/');
    page = require('./main.po');
    return promise;
  });

  it('should include event callendar', function() {
    expect(page.calendar.getAttribute('view')).to.eventually.equal('$ctrl.calendarView');
  });

  it('should include Future events list', function() {
    expect(page.eventTable.getAttribute('class')).to.eventually.contains('table');
  });
});
