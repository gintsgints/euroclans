/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/chat_messages              ->  index
 * POST    /api/chat_messages              ->  create
 * GET     /api/chat_messages/:id          ->  show
 * PUT     /api/chat_messages/:id          ->  upsert
 * PATCH   /api/chat_messages/:id          ->  patch
 * DELETE  /api/chat_messages/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import ChatMessage from './chat_message.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch (err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of ChatMessages
export function index(req, res) {
  return ChatMessage.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single ChatMessage from the DB
export function show(req, res) {
  return ChatMessage.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new ChatMessage in the DB
export function create(req, res) {
  req.body.owner = req.user._id;
  return ChatMessage.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given ChatMessage in the DB at the specified ID
export function upsert(req, res) {
  if (req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return ChatMessage.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true }).exec()

  .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing ChatMessage in the DB
export function patch(req, res) {
  if (req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return ChatMessage.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a ChatMessage from the DB
export function destroy(req, res) {
  return ChatMessage.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
