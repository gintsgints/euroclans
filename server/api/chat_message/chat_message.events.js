/**
 * ChatMessage model events
 */

'use strict';

import {EventEmitter} from 'events';
var ChatMessageEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ChatMessageEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(ChatMessage) {
  for (var e in events) {
    let event = events[e];
    ChatMessage.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    ChatMessageEvents.emit(event + ':' + doc._id, doc);
    ChatMessageEvents.emit(event, doc);
  };
}

export {registerEvents};
export default ChatMessageEvents;
