'use strict';

/* globals describe, expect, it, after, before, beforeEach, afterEach */

var app = require('../..');
import User from '../user/user.model';
import request from 'supertest';

var newChatMessage;

describe('ChatMessage API:', function() {
  var user;
  var admin;
  var token;
  var admtoken;

  // Clear users before testing
  before(function(done) {
    return User.remove().then(function() {
      user = new User({
        name: 'Fake User',
        email: 'test@example.com',
        password: 'password'
      });
      admin = new User({
        name: 'Fake Admin',
        email: 'admin@example.com',
        password: 'password',
        role: 'admin'
      });

      user.save().then(() => {
        admin.save().then(() => {
          request(app)
            .post('/auth/local')
            .send({
              email: 'test@example.com',
              password: 'password'
            })
            .expect(200)
            .expect('Content-Type', /json/)
            .end((err, res) => {
              token = res.body.token;
              request(app)
                .post('/auth/local')
                .send({
                  email: 'admin@example.com',
                  password: 'password'
                })
                .expect(200)
                .expect('Content-Type', /json/)
                .end((err, res) => {
                  admtoken = res.body.token;
                  done();
                });
            });
        });
      });
    });
  });

  // Clear users after testing
  after(function() {
    return User.remove();
  });

  describe('GET /api/chat_messages', function() {
    var chatMessages;

    beforeEach(function(done) {
      request(app)
        .get('/api/chat_messages')
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          chatMessages = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(chatMessages).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/chat_messages', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/chat_messages')
        .send({
          message: 'This is the brand new chatMessage!!!'
        })
        .set('authorization', `Bearer ${token}`)
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newChatMessage = res.body;
          done();
        });
    });

    it('should respond with the newly created chatMessage', function() {
      expect(newChatMessage.owner).to.equal(user._id.toString());
      expect(newChatMessage.message).to.equal('This is the brand new chatMessage!!!');
    });
  });

  describe('GET /api/chat_messages/:id', function() {
    var chatMessage;

    beforeEach(function(done) {
      request(app)
        .get(`/api/chat_messages/${newChatMessage._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          chatMessage = res.body;
          done();
        });
    });

    afterEach(function() {
      chatMessage = {};
    });

    it('should respond with the requested chatMessage', function() {
      expect(chatMessage.message).to.equal('This is the brand new chatMessage!!!');
    });
  });

  describe('PUT /api/chat_messages/:id', function() {
    var updatedChatMessage;

    beforeEach(function(done) {
      request(app)
        .put(`/api/chat_messages/${newChatMessage._id}`)
        .set('authorization', `Bearer ${token}`)
        .send({
          message: 'This is the updated chatMessage!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedChatMessage = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedChatMessage = {};
    });

    it('should respond with the updated chatMessage', function() {
      expect(updatedChatMessage.message).to.equal('This is the updated chatMessage!!!');
    });

    it('should respond with the updated chatMessage on a subsequent GET', function(done) {
      request(app)
        .get(`/api/chat_messages/${newChatMessage._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          let chatMessage = res.body;

          expect(chatMessage.message).to.equal('This is the updated chatMessage!!!');

          done();
        });
    });
  });

  describe('PATCH /api/chat_messages/:id', function() {
    var patchedChatMessage;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/chat_messages/${newChatMessage._id}`)
        .set('authorization', `Bearer ${token}`)
        .send([
          { op: 'replace', path: '/message', value: 'This is the patched chatMessage!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          patchedChatMessage = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedChatMessage = {};
    });

    it('should respond with the patched chatMessage', function() {
      expect(patchedChatMessage.message).to.equal('This is the patched chatMessage!!!');
    });
  });

  describe('DELETE /api/chat_messages/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/chat_messages/${newChatMessage._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(204)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 403 - forbidden when chatMessage does not exist', function(done) {
      request(app)
        .delete(`/api/chat_messages/${newChatMessage._id}`)
        .set('authorization', `Bearer ${admtoken}`)
        .expect(403)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });
});
