'use strict';

import mongoose from 'mongoose';
import { registerEvents } from './chat_message.events';

var ChatMessageSchema = new mongoose.Schema({
  message: String,
  user: { type: mongoose.Schema.ObjectId, ref: 'User' },
  owner: { type: mongoose.Schema.ObjectId, ref: 'User' },
});

registerEvents(ChatMessageSchema);
export default mongoose.model('ChatMessage', ChatMessageSchema);
