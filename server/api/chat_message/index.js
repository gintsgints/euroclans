'use strict';

var express = require('express');
var controller = require('./chat_message.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.ownsObject('ChatMessage'), controller.upsert);
router.patch('/:id', auth.ownsObject('ChatMessage'), controller.patch);
router.delete('/:id', auth.ownsObject('ChatMessage'), controller.destroy);

module.exports = router;
