'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var chatMessageCtrlStub = {
  index: 'chatMessageCtrl.index',
  show: 'chatMessageCtrl.show',
  create: 'chatMessageCtrl.create',
  upsert: 'chatMessageCtrl.upsert',
  patch: 'chatMessageCtrl.patch',
  destroy: 'chatMessageCtrl.destroy'
};

var authServiceStub = {
  isAuthenticated() {
    return 'authService.isAuthenticated';
  },
  hasRole(role) {
    return `authService.hasRole.${role}`;
  },
  ownsObject(objname) {
    return `authService.ownsObject.${objname}`;
  }
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var chatMessageIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './chat_message.controller': chatMessageCtrlStub,
  '../../auth/auth.service': authServiceStub
});

describe('ChatMessage API Router:', function() {
  it('should return an express router instance', function() {
    expect(chatMessageIndex).to.equal(routerStub);
  });

  describe('GET /api/chat_messages', function() {
    it('should route to chatMessage.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'authService.isAuthenticated', 'chatMessageCtrl.index')
      ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/chat_messages/:id', function() {
    it('should route to chatMessage.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'authService.isAuthenticated', 'chatMessageCtrl.show')
      ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/chat_messages', function() {
    it('should route to chatMessage.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'authService.isAuthenticated', 'chatMessageCtrl.create')
      ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/chat_messages/:id', function() {
    it('should route to chatMessage.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'authService.ownsObject.ChatMessage', 'chatMessageCtrl.upsert')
      ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/chat_messages/:id', function() {
    it('should route to chatMessage.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'authService.ownsObject.ChatMessage', 'chatMessageCtrl.patch')
      ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/chat_messages/:id', function() {
    it('should route to chatMessage.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'authService.ownsObject.ChatMessage', 'chatMessageCtrl.destroy')
      ).to.have.been.calledOnce;
    });
  });
});
