/**
 * Clan model events
 */

'use strict';

import { EventEmitter } from 'events';
var ClanEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ClanEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Clan) {
  for (var e in events) {
    let event = events[e];
    Clan.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    ClanEvents.emit('${event}:${doc._id}', doc);
    ClanEvents.emit(event, doc);
  };
}

export { registerEvents };
export default ClanEvents;
