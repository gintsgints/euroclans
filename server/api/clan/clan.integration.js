'use strict';

/* globals describe, expect, it, after, before, beforeEach, afterEach */

var app = require('../..');
import User from '../user/user.model';
import request from 'supertest';

var newClan;

describe('Clan API:', function() {
  var user;
  var admin;
  var token;
  var admtoken;

  // Clear users before testing
  before(function(done) {
    return User.remove().then(function() {
      user = new User({
        name: 'Fake User',
        email: 'test@example.com',
        password: 'password'
      });
      admin = new User({
        name: 'Fake Admin',
        email: 'admin@example.com',
        password: 'password',
        role: 'admin'
      });

      user.save().then(() => {
        admin.save().then(() => {
          request(app)
            .post('/auth/local')
            .send({
              email: 'test@example.com',
              password: 'password'
            })
            .expect(200)
            .expect('Content-Type', /json/)
            .end((err, res) => {
              token = res.body.token;
              request(app)
                .post('/auth/local')
                .send({
                  email: 'admin@example.com',
                  password: 'password'
                })
                .expect(200)
                .expect('Content-Type', /json/)
                .end((err, res) => {
                  admtoken = res.body.token;
                  done();
                });
            });
        });
      });
    });
  });

  // Clear users after testing
  after(function() {
    return User.remove();
  });

  describe('GET /api/clans', function() {
    var clans;

    beforeEach(function(done) {
      request(app)
        .get('/api/clans')
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          clans = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(clans).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/clans', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/clans')
        .set('authorization', `Bearer ${token}`)
        .send({
          name: 'New Clan',
          info: 'This is the brand new clan!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newClan = res.body;
          done();
        });
    });

    it('should respond with the newly created clan', function() {
      expect(newClan.name).to.equal('New Clan');
      expect(newClan.info).to.equal('This is the brand new clan!!!');
    });
  });

  describe('GET /api/clans/:id', function() {
    var clan;

    beforeEach(function(done) {
      request(app)
        .get(`/api/clans/${newClan._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          clan = res.body;
          done();
        });
    });

    afterEach(function() {
      clan = {};
    });

    it('should respond with the requested clan', function() {
      expect(clan.name).to.equal('New Clan');
      expect(clan.info).to.equal('This is the brand new clan!!!');
    });
  });

  describe('PUT /api/clans/:id', function() {
    var updatedClan;

    beforeEach(function(done) {
      request(app)
        .put(`/api/clans/${newClan._id}`)
        .send({
          name: 'Updated Clan',
          info: 'This is the updated clan!!!'
        })
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedClan = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedClan = {};
    });

    it('should respond with the updated clan', function() {
      expect(updatedClan.name).to.equal('Updated Clan');
      expect(updatedClan.info).to.equal('This is the updated clan!!!');
    });

    it('should respond with the updated clan on a subsequent GET', function(done) {
      request(app)
        .get(`/api/clans/${newClan._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          let clan = res.body;

          expect(clan.name).to.equal('Updated Clan');
          expect(clan.info).to.equal('This is the updated clan!!!');

          done();
        });
    });
  });

  describe('PATCH /api/clans/:id', function() {
    var patchedClan;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/clans/${newClan._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Clan' },
          { op: 'replace', path: '/info', value: 'This is the patched clan!!!' }
        ])
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          patchedClan = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedClan = {};
    });

    it('should respond with the patched clan', function() {
      expect(patchedClan.name).to.equal('Patched Clan');
      expect(patchedClan.info).to.equal('This is the patched clan!!!');
    });
  });

  describe('DELETE /api/clans/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/clans/${newClan._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(204)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 403 when clan does not exist', function(done) {
      request(app)
        .delete(`/api/clans/${newClan._id}`)
        .set('authorization', `Bearer ${admtoken}`)
        .expect(403)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });
});
