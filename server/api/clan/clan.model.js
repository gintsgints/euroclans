'use strict';

import mongoose from 'mongoose';
import Game from '../game/game.model';
import GamePlatform from '../game_platform/game_platform.model';
import { registerEvents } from './clan.events';
import ChatMessage from '../chat_message/chat_message.model';

var ClanSchema = new mongoose.Schema({
  name: String,
  tag: String,
  info: String,
  homepage: String,
  img: String,
  game: Game.schema,
  gameplatform: GamePlatform.schema,
  joinpassword: String,
  active: Boolean,
  recruitment: Boolean,
  chat: [ChatMessage.schema],
  members: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
  owner: { type: mongoose.Schema.ObjectId, ref: 'User' },
});

registerEvents(ClanSchema);
export default mongoose.model('Clan', ClanSchema);
