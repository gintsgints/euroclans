'use strict';

var express = require('express');
var controller = require('./clan.controller');
import * as auth from '../../auth/auth.service';
import * as mail from '../../components/mail/mail.service';

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.ownsObject('Clan'), controller.upsert);
router.patch('/:id', auth.ownsObject('Clan', ['/chat/0']), mail.sendClanEmail(), controller.patch);
router.delete('/:id', auth.ownsObject('Clan'), controller.destroy);

module.exports = router;
