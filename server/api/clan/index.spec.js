'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var clanCtrlStub = {
  index: 'clanCtrl.index',
  show: 'clanCtrl.show',
  create: 'clanCtrl.create',
  upsert: 'clanCtrl.upsert',
  patch: 'clanCtrl.patch',
  destroy: 'clanCtrl.destroy'
};

var authServiceStub = {
  isAuthenticated() {
    return 'authService.isAuthenticated';
  },
  hasRole(role) {
    return `authService.hasRole.${role}`;
  },
  ownsObject(objname, patches) {
    var retval = `authService.ownsObject.${objname}`;
    if (patches) {
      patches.forEach(element => {
        retval = retval.concat('.', element);
      }, this);
    }
    return retval;
  }
};

var mailServiceStub = {
  sendClanEmail() {
    return 'mailService.sendClanEmail';
  }
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var clanIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './clan.controller': clanCtrlStub,
  '../../auth/auth.service': authServiceStub,
  '../../components/mail/mail.service': mailServiceStub
});

describe('Clan API Router:', function() {
  it('should return an express router instance', function() {
    expect(clanIndex).to.equal(routerStub);
  });

  describe('GET /api/clans', function() {
    it('should route to clan.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'clanCtrl.index')
      ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/clans/:id', function() {
    it('should route to clan.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'clanCtrl.show')
      ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/clans', function() {
    it('should route to clan.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'authService.isAuthenticated', 'clanCtrl.create')
      ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/clans/:id', function() {
    it('should route to clan.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'authService.ownsObject.Clan', 'clanCtrl.upsert')
      ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/clans/:id', function() {
    it('should route to clan.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'authService.ownsObject.Clan./chat/0', 'mailService.sendClanEmail', 'clanCtrl.patch')
      ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/clans/:id', function() {
    it('should route to clan.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'authService.ownsObject.Clan', 'clanCtrl.destroy')
      ).to.have.been.calledOnce;
    });
  });
});
