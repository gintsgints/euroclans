'use strict';

import mongoose from 'mongoose';
import { registerEvents } from './event.events';
import Game from '../game/game.model';
import GameType from '../game_type/game_type.model';
import GamePlatform from '../game_platform/game_platform.model';
import GameMap from '../game_map/game_map.model';
import ChatMessage from '../chat_message/chat_message.model';
import Clan from '../clan/clan.model';

var EventSchema = new mongoose.Schema({
  name: String,
  datetime: Date,
  info: String,
  game: Game.schema,
  gametype: GameType.schema,
  gameplatform: GamePlatform.schema,
  gameMaps: [GameMap.schema],
  refLink: String,
  leftTeam: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
  rightTeam: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
  leftClan: Clan.schema,
  rightClan: Clan.schema,
  resultLeft: Number,
  resultRight: Number,
  confirmedLeft: Boolean,
  confirmedRight: Boolean,
  finished: Boolean,
  chat: [ChatMessage.schema],
  active: Boolean,
  owner: { type: mongoose.Schema.ObjectId, ref: 'User' },
});

registerEvents(EventSchema);
export default mongoose.model('Event', EventSchema);
