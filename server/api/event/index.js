'use strict';

var express = require('express');
var controller = require('./event.controller');
import * as auth from '../../auth/auth.service';
import * as mail from '../../components/mail/mail.service';

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', auth.isAuthenticated(), mail.sendNewEventEmail(), controller.create);
router.put('/:id', auth.inEventTeam('Event'), controller.upsert);
router.patch('/:id', auth.ownsObject('Event', ['/chat/0', '/leftTeam/0', '/rightTeam/0', '/gameMaps']), mail.sendEventPatchEmail(), controller.patch);
router.delete('/:id', auth.ownsObject('Event'), controller.destroy);

module.exports = router;
