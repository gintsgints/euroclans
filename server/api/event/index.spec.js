'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var eventCtrlStub = {
  index: 'eventCtrl.index',
  show: 'eventCtrl.show',
  create: 'eventCtrl.create',
  upsert: 'eventCtrl.upsert',
  patch: 'eventCtrl.patch',
  destroy: 'eventCtrl.destroy'
};

var authServiceStub = {
  isAuthenticated() {
    return 'authService.isAuthenticated';
  },
  hasRole(role) {
    return `authService.hasRole.${role}`;
  },
  inEventTeam(objname) {
    return `authService.inEventTeam.${objname}`;
  },
  ownsObject(objname, patches) {
    var retval = `authService.ownsObject.${objname}`;
    if (patches) {
      patches.forEach(element => {
        retval = retval.concat('.', element);
      }, this);
    }
    return retval;
  }
};


var mailServiceStub = {
  sendEventPatchEmail() {
    return 'mailService.sendEventPatchEmail';
  }
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var eventIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './event.controller': eventCtrlStub,
  '../../auth/auth.service': authServiceStub,
  '../../components/mail/mail.service': mailServiceStub
});

describe('Event API Router:', function() {
  it('should return an express router instance', function() {
    expect(eventIndex).to.equal(routerStub);
  });

  describe('GET /api/events', function() {
    it('should route to event.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'eventCtrl.index')
      ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/events/:id', function() {
    it('should route to event.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'eventCtrl.show')
      ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/events', function() {
    it('should route to event.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'authService.isAuthenticated', 'eventCtrl.create')
      ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/events/:id', function() {
    it('should route to event.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'authService.inEventTeam.Event', 'eventCtrl.upsert')
      ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/events/:id', function() {
    it('should route to event.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'authService.ownsObject.Event./chat/0./leftTeam/0./rightTeam/0', 'mailService.sendEventPatchEmail', 'eventCtrl.patch')
      ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/events/:id', function() {
    it('should route to event.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'authService.ownsObject.Event', 'eventCtrl.destroy')
      ).to.have.been.calledOnce;
    });
  });
});
