'use strict';

/* globals describe, expect, it, after, before, beforeEach, afterEach */

var app = require('../..');
import User from '../user/user.model';
import request from 'supertest';

var newGame;

describe('Game API:', function() {
  var user;
  var admin;
  var token;
  var admtoken;

  // Clear users before testing
  before(function(done) {
    return User.remove().then(function() {
      user = new User({
        name: 'Fake User',
        email: 'test@example.com',
        password: 'password'
      });
      admin = new User({
        name: 'Fake Admin',
        email: 'admin@example.com',
        password: 'password',
        role: 'admin'
      });

      user.save().then(() => {
        admin.save().then(() => {
          request(app)
            .post('/auth/local')
            .send({
              email: 'test@example.com',
              password: 'password'
            })
            .expect(200)
            .expect('Content-Type', /json/)
            .end((err, res) => {
              token = res.body.token;
              request(app)
                .post('/auth/local')
                .send({
                  email: 'admin@example.com',
                  password: 'password'
                })
                .expect(200)
                .expect('Content-Type', /json/)
                .end((err, res) => {
                  admtoken = res.body.token;
                  done();
                });
            });
        });
      });
    });
  });

  // Clear users after testing
  after(function() {
    return User.remove();
  });

  describe('GET /api/games', function() {
    var games;

    beforeEach(function(done) {
      request(app)
        .get('/api/games')
        .expect(200)
        .expect('Content-Type', /json/)
        .set('authorization', `Bearer ${token}`)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          games = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(games).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/games', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/games')
        .send({
          name: 'New Game',
          info: 'This is the brand new game!!!'
        })
        .set('authorization', `Bearer ${admtoken}`)
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newGame = res.body;
          done();
        });
    });

    it('should respond with the newly created game', function() {
      expect(newGame.name).to.equal('New Game');
      expect(newGame.info).to.equal('This is the brand new game!!!');
    });
  });

  describe('GET /api/games/:id', function() {
    var game;

    beforeEach(function(done) {
      request(app)
        .get(`/api/games/${newGame._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          game = res.body;
          done();
        });
    });

    afterEach(function() {
      game = {};
    });

    it('should respond with the requested game', function() {
      expect(game.name).to.equal('New Game');
      expect(game.info).to.equal('This is the brand new game!!!');
    });
  });

  describe('PUT /api/games/:id', function() {
    var updatedGame;

    beforeEach(function(done) {
      request(app)
        .put(`/api/games/${newGame._id}`)
        .send({
          name: 'Updated Game',
          info: 'This is the updated game!!!'
        })
        .set('authorization', `Bearer ${admtoken}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedGame = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedGame = {};
    });

    it('should respond with the updated game', function() {
      expect(updatedGame.name).to.equal('Updated Game');
      expect(updatedGame.info).to.equal('This is the updated game!!!');
    });

    it('should respond with the updated game on a subsequent GET', function(done) {
      request(app)
        .get(`/api/games/${newGame._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          let game = res.body;

          expect(game.name).to.equal('Updated Game');
          expect(game.info).to.equal('This is the updated game!!!');

          done();
        });
    });
  });

  describe('PATCH /api/games/:id', function() {
    var patchedGame;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/games/${newGame._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Game' },
          { op: 'replace', path: '/info', value: 'This is the patched game!!!' }
        ])
        .set('authorization', `Bearer ${admtoken}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          patchedGame = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedGame = {};
    });

    it('should respond with the patched game', function() {
      expect(patchedGame.name).to.equal('Patched Game');
      expect(patchedGame.info).to.equal('This is the patched game!!!');
    });
  });

  describe('DELETE /api/games/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/games/${newGame._id}`)
        .set('authorization', `Bearer ${admtoken}`)
        .expect(204)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when game does not exist', function(done) {
      request(app)
        .delete(`/api/games/${newGame._id}`)
        .set('authorization', `Bearer ${admtoken}`)
        .expect(404)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });
});
