'use strict';

import mongoose from 'mongoose';
import { registerEvents } from './game.events';
import GameType from '../game_type/game_type.model';
import GamePlatform from '../game_platform/game_platform.model';
import GameMap from '../game_map/game_map.model';

var GameSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean,
  img: String,
  gameTypes: [GameType.schema],
  gamePlatforms: [GamePlatform.schema],
  gameMaps: [GameMap.schema]
});

registerEvents(GameSchema);
export default mongoose.model('Game', GameSchema);
