'use strict';

import mongoose from 'mongoose';

var GameMapSchema = new mongoose.Schema({
  name: String,
  img: String,
  active: Boolean
});

export default mongoose.model('GameMap', GameMapSchema);
