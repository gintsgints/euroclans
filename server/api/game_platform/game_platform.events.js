/**
 * GamePlatform model events
 */

'use strict';

import { EventEmitter } from 'events';
var GamePlatformEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
GamePlatformEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(GamePlatform) {
  for (var e in events) {
    let event = events[e];
    GamePlatform.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    GamePlatformEvents.emit('${event}:${doc._id}', doc);
    GamePlatformEvents.emit(event, doc);
  };
}

export { registerEvents };
export default GamePlatformEvents;
