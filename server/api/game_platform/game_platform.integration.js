'use strict';

/* globals describe, expect, it, after, before, beforeEach, afterEach */

var app = require('../..');
import User from '../user/user.model';
import request from 'supertest';

var newGamePlatform;

describe('GamePlatform API:', function() {
  var user;
  var admin;
  var token;
  var admtoken;

  // Clear users before testing
  before(function(done) {
    return User.remove().then(function() {
      user = new User({
        name: 'Fake User',
        email: 'test@example.com',
        password: 'password'
      });
      admin = new User({
        name: 'Fake Admin',
        email: 'admin@example.com',
        password: 'password',
        role: 'admin'
      });

      user.save().then(() => {
        admin.save().then(() => {
          request(app)
            .post('/auth/local')
            .send({
              email: 'test@example.com',
              password: 'password'
            })
            .expect(200)
            .expect('Content-Type', /json/)
            .end((err, res) => {
              token = res.body.token;
              request(app)
                .post('/auth/local')
                .send({
                  email: 'admin@example.com',
                  password: 'password'
                })
                .expect(200)
                .expect('Content-Type', /json/)
                .end((err, res) => {
                  admtoken = res.body.token;
                  done();
                });
            });
        });
      });
    });
  });

  // Clear users after testing
  after(function() {
    return User.remove();
  });

  describe('GET /api/game_platforms', function() {
    var gamePlatforms;

    beforeEach(function(done) {
      request(app)
        .get('/api/game_platforms')
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          gamePlatforms = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(gamePlatforms).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/game_platforms', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/game_platforms')
        .send({
          name: 'New GamePlatform'
        })
        .set('authorization', `Bearer ${admtoken}`)
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newGamePlatform = res.body;
          done();
        });
    });

    it('should respond with the newly created gamePlatform', function() {
      expect(newGamePlatform.name).to.equal('New GamePlatform');
    });
  });

  describe('GET /api/game_platforms/:id', function() {
    var gamePlatform;

    beforeEach(function(done) {
      request(app)
        .get(`/api/game_platforms/${newGamePlatform._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          gamePlatform = res.body;
          done();
        });
    });

    afterEach(function() {
      gamePlatform = {};
    });

    it('should respond with the requested gamePlatform', function() {
      expect(gamePlatform.name).to.equal('New GamePlatform');
    });
  });

  describe('PUT /api/game_platforms/:id', function() {
    var updatedGamePlatform;

    beforeEach(function(done) {
      request(app)
        .put(`/api/game_platforms/${newGamePlatform._id}`)
        .send({
          name: 'Updated GamePlatform'
        })
        .set('authorization', `Bearer ${admtoken}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedGamePlatform = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedGamePlatform = {};
    });

    it('should respond with the updated gamePlatform', function() {
      expect(updatedGamePlatform.name).to.equal('Updated GamePlatform');
    });

    it('should respond with the updated gamePlatform on a subsequent GET', function(done) {
      request(app)
        .get(`/api/game_platforms/${newGamePlatform._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          let gamePlatform = res.body;

          expect(gamePlatform.name).to.equal('Updated GamePlatform');

          done();
        });
    });
  });

  describe('PATCH /api/game_platforms/:id', function() {
    var patchedGamePlatform;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/game_platforms/${newGamePlatform._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched GamePlatform' }
        ])
        .set('authorization', `Bearer ${admtoken}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          patchedGamePlatform = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedGamePlatform = {};
    });

    it('should respond with the patched gamePlatform', function() {
      expect(patchedGamePlatform.name).to.equal('Patched GamePlatform');
    });
  });

  describe('DELETE /api/game_platforms/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/game_platforms/${newGamePlatform._id}`)
        .set('authorization', `Bearer ${admtoken}`)
        .expect(204)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when gamePlatform does not exist', function(done) {
      request(app)
        .delete(`/api/game_platforms/${newGamePlatform._id}`)
        .set('authorization', `Bearer ${admtoken}`)
        .expect(404)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });
});
