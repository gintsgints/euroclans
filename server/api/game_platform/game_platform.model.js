'use strict';

import mongoose from 'mongoose';
import { registerEvents } from './game_platform.events';

var GamePlatformSchema = new mongoose.Schema({
  name: String,
  img: String,
  active: Boolean
});

registerEvents(GamePlatformSchema);
export default mongoose.model('GamePlatform', GamePlatformSchema);
