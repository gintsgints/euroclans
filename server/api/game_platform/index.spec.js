'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var gamePlatformCtrlStub = {
  index: 'gamePlatformCtrl.index',
  show: 'gamePlatformCtrl.show',
  create: 'gamePlatformCtrl.create',
  upsert: 'gamePlatformCtrl.upsert',
  patch: 'gamePlatformCtrl.patch',
  destroy: 'gamePlatformCtrl.destroy'
};

var authServiceStub = {
  isAuthenticated() {
    return 'authService.isAuthenticated';
  },
  hasRole(role) {
    return `authService.hasRole.${role}`;
  },
  ownsObject(objname) {
    return `authService.ownsObject.${objname}`;
  }
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var gamePlatformIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './game_platform.controller': gamePlatformCtrlStub,
  '../../auth/auth.service': authServiceStub
});

describe('GamePlatform API Router:', function() {
  it('should return an express router instance', function() {
    expect(gamePlatformIndex).to.equal(routerStub);
  });

  describe('GET /api/game_platforms', function() {
    it('should route to gamePlatform.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'gamePlatformCtrl.index')
      ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/game_platforms/:id', function() {
    it('should route to gamePlatform.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'gamePlatformCtrl.show')
      ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/game_platforms', function() {
    it('should route to gamePlatform.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'authService.hasRole.admin', 'gamePlatformCtrl.create')
      ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/game_platforms/:id', function() {
    it('should route to gamePlatform.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'authService.hasRole.admin', 'gamePlatformCtrl.upsert')
      ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/game_platforms/:id', function() {
    it('should route to gamePlatform.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'authService.hasRole.admin', 'gamePlatformCtrl.patch')
      ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/game_platforms/:id', function() {
    it('should route to gamePlatform.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'authService.hasRole.admin', 'gamePlatformCtrl.destroy')
      ).to.have.been.calledOnce;
    });
  });
});
