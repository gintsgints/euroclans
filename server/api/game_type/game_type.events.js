/**
 * GameType model events
 */

'use strict';

import { EventEmitter } from 'events';
var GameTypeEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
GameTypeEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(GameType) {
  for (var e in events) {
    let event = events[e];
    GameType.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    GameTypeEvents.emit('${event}:${doc._id}', doc);
    GameTypeEvents.emit(event, doc);
  };
}

export { registerEvents };
export default GameTypeEvents;
