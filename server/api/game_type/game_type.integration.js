'use strict';

/* globals describe, expect, it, after, before, beforeEach, afterEach */

var app = require('../..');
import User from '../user/user.model';
import request from 'supertest';

var newGameType;

describe('GameType API:', function() {
  var user;
  var admin;
  var token;
  var admtoken;

  // Clear users before testing
  before(function(done) {
    return User.remove().then(function() {
      user = new User({
        name: 'Fake User',
        email: 'test@example.com',
        password: 'password'
      });
      admin = new User({
        name: 'Fake Admin',
        email: 'admin@example.com',
        password: 'password',
        role: 'admin'
      });

      user.save().then(() => {
        admin.save().then(() => {
          request(app)
            .post('/auth/local')
            .send({
              email: 'test@example.com',
              password: 'password'
            })
            .expect(200)
            .expect('Content-Type', /json/)
            .end((err, res) => {
              token = res.body.token;
              request(app)
                .post('/auth/local')
                .send({
                  email: 'admin@example.com',
                  password: 'password'
                })
                .expect(200)
                .expect('Content-Type', /json/)
                .end((err, res) => {
                  admtoken = res.body.token;
                  done();
                });
            });
        });
      });
    });
  });

  // Clear users after testing
  after(function() {
    return User.remove();
  });

  describe('GET /api/game_types', function() {
    var gameTypes;

    beforeEach(function(done) {
      request(app)
        .get('/api/game_types')
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          gameTypes = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(gameTypes).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/game_types', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/game_types')
        .send({
          name: 'New GameType'
        })
        .set('authorization', `Bearer ${admtoken}`)
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newGameType = res.body;
          done();
        });
    });

    it('should respond with the newly created gameType', function() {
      expect(newGameType.name).to.equal('New GameType');
    });
  });

  describe('GET /api/game_types/:id', function() {
    var gameType;

    beforeEach(function(done) {
      request(app)
        .get(`/api/game_types/${newGameType._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          gameType = res.body;
          done();
        });
    });

    afterEach(function() {
      gameType = {};
    });

    it('should respond with the requested gameType', function() {
      expect(gameType.name).to.equal('New GameType');
    });
  });

  describe('PUT /api/game_types/:id', function() {
    var updatedGameType;

    beforeEach(function(done) {
      request(app)
        .put(`/api/game_types/${newGameType._id}`)
        .send({
          name: 'Updated GameType'
        })
        .set('authorization', `Bearer ${admtoken}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedGameType = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedGameType = {};
    });

    it('should respond with the updated gameType', function() {
      expect(updatedGameType.name).to.equal('Updated GameType');
    });

    it('should respond with the updated gameType on a subsequent GET', function(done) {
      request(app)
        .get(`/api/game_types/${newGameType._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          let gameType = res.body;

          expect(gameType.name).to.equal('Updated GameType');

          done();
        });
    });
  });

  describe('PATCH /api/game_types/:id', function() {
    var patchedGameType;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/game_types/${newGameType._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched GameType' }
        ])
        .set('authorization', `Bearer ${admtoken}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          patchedGameType = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedGameType = {};
    });

    it('should respond with the patched gameType', function() {
      expect(patchedGameType.name).to.equal('Patched GameType');
    });
  });

  describe('DELETE /api/game_types/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/game_types/${newGameType._id}`)
        .set('authorization', `Bearer ${admtoken}`)
        .expect(204)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when gameType does not exist', function(done) {
      request(app)
        .delete(`/api/game_types/${newGameType._id}`)
        .set('authorization', `Bearer ${admtoken}`)
        .expect(404)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });
});
