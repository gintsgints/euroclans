'use strict';

import mongoose from 'mongoose';
import { registerEvents } from './game_type.events';

var GameTypeSchema = new mongoose.Schema({
  name: String,
  active: Boolean
});

registerEvents(GameTypeSchema);
export default mongoose.model('GameType', GameTypeSchema);
