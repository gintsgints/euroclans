'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var gameTypeCtrlStub = {
  index: 'gameTypeCtrl.index',
  show: 'gameTypeCtrl.show',
  create: 'gameTypeCtrl.create',
  upsert: 'gameTypeCtrl.upsert',
  patch: 'gameTypeCtrl.patch',
  destroy: 'gameTypeCtrl.destroy'
};

var authServiceStub = {
  isAuthenticated() {
    return 'authService.isAuthenticated';
  },
  hasRole(role) {
    return `authService.hasRole.${role}`;
  },
  ownsObject(objname) {
    return `authService.ownsObject.${objname}`;
  }
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var gameTypeIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './game_type.controller': gameTypeCtrlStub,
  '../../auth/auth.service': authServiceStub
});

describe('GameType API Router:', function() {
  it('should return an express router instance', function() {
    expect(gameTypeIndex).to.equal(routerStub);
  });

  describe('GET /api/game_types', function() {
    it('should route to gameType.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'gameTypeCtrl.index')
      ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/game_types/:id', function() {
    it('should route to gameType.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'gameTypeCtrl.show')
      ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/game_types', function() {
    it('should route to gameType.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'authService.hasRole.admin', 'gameTypeCtrl.create')
      ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/game_types/:id', function() {
    it('should route to gameType.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'authService.hasRole.admin', 'gameTypeCtrl.upsert')
      ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/game_types/:id', function() {
    it('should route to gameType.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'authService.hasRole.admin', 'gameTypeCtrl.patch')
      ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/game_types/:id', function() {
    it('should route to gameType.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'authService.hasRole.admin', 'gameTypeCtrl.destroy')
      ).to.have.been.calledOnce;
    });
  });
});
