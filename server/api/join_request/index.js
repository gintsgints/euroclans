'use strict';

var express = require('express');
var controller = require('./join_request.controller');
import * as auth from '../../auth/auth.service';
import * as mail from '../../components/mail/mail.service';

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), mail.sendJoinRequestEmail(), controller.create);
router.put('/:id', auth.ownsObject('JoinRequest'), controller.upsert);
router.patch('/:id', auth.ownsObject('JoinRequest'), controller.patch);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;
