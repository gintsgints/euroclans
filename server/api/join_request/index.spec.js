'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var joinRequestCtrlStub = {
  index: 'joinRequestCtrl.index',
  show: 'joinRequestCtrl.show',
  create: 'joinRequestCtrl.create',
  upsert: 'joinRequestCtrl.upsert',
  patch: 'joinRequestCtrl.patch',
  destroy: 'joinRequestCtrl.destroy'
};

var authServiceStub = {
  isAuthenticated() {
    return 'authService.isAuthenticated';
  },
  hasRole(role) {
    return `authService.hasRole.${role}`;
  },
  ownsObject(objname) {
    return `authService.ownsObject.${objname}`;
  }
};

var mailServiceStub = {
  sendJoinRequestEmail() {
    return 'mailService.sendJoinRequestEmail';
  }
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var joinRequestIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './join_request.controller': joinRequestCtrlStub,
  '../../auth/auth.service': authServiceStub,
  '../../components/mail/mail.service': mailServiceStub
});

describe('JoinRequest API Router:', function() {
  it('should return an express router instance', function() {
    expect(joinRequestIndex).to.equal(routerStub);
  });

  describe('GET /api/join_requests', function() {
    it('should route to joinRequest.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'authService.isAuthenticated', 'joinRequestCtrl.index')
      ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/join_requests/:id', function() {
    it('should route to joinRequest.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'authService.isAuthenticated', 'joinRequestCtrl.show')
      ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/join_requests', function() {
    it('should route to joinRequest.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'authService.isAuthenticated', 'mailService.sendJoinRequestEmail', 'joinRequestCtrl.create')
      ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/join_requests/:id', function() {
    it('should route to joinRequest.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'authService.ownsObject.JoinRequest', 'joinRequestCtrl.upsert')
      ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/join_requests/:id', function() {
    it('should route to joinRequest.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'authService.ownsObject.JoinRequest', 'joinRequestCtrl.patch')
      ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/join_requests/:id', function() {
    it('should route to joinRequest.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'authService.isAuthenticated', 'joinRequestCtrl.destroy')
      ).to.have.been.calledOnce;
    });
  });
});
