/**
 * JoinRequest model events
 */

'use strict';

import { EventEmitter } from 'events';
var JoinRequestEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
JoinRequestEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(JoinRequest) {
  for (var e in events) {
    let event = events[e];
    JoinRequest.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    JoinRequestEvents.emit('${event}:${doc._id}', doc);
    JoinRequestEvents.emit(event, doc);
  };
}

export { registerEvents };
export default JoinRequestEvents;
