'use strict';

/* globals describe, expect, it, after, before, beforeEach, afterEach */

var app = require('../..');
import User from '../user/user.model';
import Clan from '../clan/clan.model';
import request from 'supertest';

var newJoinRequest;

describe('JoinRequest API:', function() {
  var user;
  var admin;
  var clan;
  var token;
  var admtoken;

  // Clear users before testing
  before(function(done) {
    return User.remove().then(function() {
      user = new User({
        name: 'Fake User',
        email: 'test@example.com',
        password: 'password'
      });
      admin = new User({
        name: 'Fake Admin',
        email: 'admin@example.com',
        password: 'password',
        role: 'admin'
      });

      user.save().then(() => {
        admin.save().then(() => {
          clan = new Clan({
            name: 'Test Clan 1',
            owner: user._id,
            tag: 'TG',
            info: 'Testing Clan for development needs. This holds long text to analyze how clan object works.',
            homepage: 'http://www.euroclans.eu',
            img: 'http://img05.deviantart.net/1357/i/2015/019/9/6/myo_clan_logo_by_littleboyblack-d5v4rot.jpg',
            members: [user._id],
          });

          clan.save().then(() => {
            request(app)
              .post('/auth/local')
              .send({
                email: 'test@example.com',
                password: 'password'
              })
              .expect(200)
              .expect('Content-Type', /json/)
              .end((err, res) => {
                token = res.body.token;
                request(app)
                  .post('/auth/local')
                  .send({
                    email: 'admin@example.com',
                    password: 'password'
                  })
                  .expect(200)
                  .expect('Content-Type', /json/)
                  .end((err, res) => {
                    admtoken = res.body.token;
                    done();
                  });
              });
          });
        });
      });
    });
  });

  // Clear users after testing
  after(function() {
    return User.remove();
  });

  describe('GET /api/join_requests', function() {
    var joinRequests;

    beforeEach(function(done) {
      request(app)
        .get('/api/join_requests')
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          joinRequests = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(joinRequests).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/join_requests', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/join_requests')
        .send({
          user,
          clan: clan._id
        })
        .set('authorization', `Bearer ${token}`)
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newJoinRequest = res.body;
          done();
        });
    });

    it('should respond with the newly created joinRequest', function() {
      expect(newJoinRequest.user).to.equal(user._id.toString());
    });
  });

  describe('GET /api/join_requests/:id', function() {
    var joinRequest;

    beforeEach(function(done) {
      request(app)
        .get(`/api/join_requests/${newJoinRequest._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          joinRequest = res.body;
          done();
        });
    });

    afterEach(function() {
      joinRequest = {};
    });

    it('should respond with the requested joinRequest', function() {
      expect(newJoinRequest.user).to.equal(user._id.toString());
    });
  });

  describe('PUT /api/join_requests/:id', function() {
    var updatedJoinRequest;

    beforeEach(function(done) {
      request(app)
        .put(`/api/join_requests/${newJoinRequest._id}`)
        .send({
          user: admin,
          clan: clan._id
        })
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedJoinRequest = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedJoinRequest = {};
    });

    it('should respond with the updated joinRequest', function() {
      expect(updatedJoinRequest.user).to.equal(admin._id.toString());
    });

    it('should respond with the updated joinRequest on a subsequent GET', function(done) {
      request(app)
        .get(`/api/join_requests/${newJoinRequest._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          let joinRequest = res.body;

          expect(joinRequest.user).to.equal(admin._id.toString());

          done();
        });
    });
  });

  describe('PATCH /api/join_requests/:id', function() {
    var patchedJoinRequest;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/join_requests/${newJoinRequest._id}`)
        .send([
          { op: 'replace', path: '/user', value: user }
        ])
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          patchedJoinRequest = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedJoinRequest = {};
    });

    it('should respond with the patched joinRequest', function() {
      expect(patchedJoinRequest.user).to.equal(user._id.toString());
    });
  });

  describe('DELETE /api/join_requests/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/join_requests/${newJoinRequest._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(204)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when joinRequest does not exist', function(done) {
      request(app)
        .delete(`/api/join_requests/${newJoinRequest._id}`)
        .set('authorization', `Bearer ${admtoken}`)
        .expect(404)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });
});
