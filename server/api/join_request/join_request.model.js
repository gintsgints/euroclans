'use strict';

import mongoose from 'mongoose';
import { registerEvents } from './join_request.events';

var JoinRequestSchema = new mongoose.Schema({
  owner: { type: mongoose.Schema.ObjectId, ref: 'User' },
  user: { type: mongoose.Schema.ObjectId, ref: 'User' },
  clan: { type: mongoose.Schema.ObjectId, ref: 'Clan' }
});

registerEvents(JoinRequestSchema);
export default mongoose.model('JoinRequest', JoinRequestSchema);
