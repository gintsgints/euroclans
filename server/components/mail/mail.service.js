import compose from 'composable-middleware';

import Clan from '../../api/clan/clan.model';
import Event from '../../api/event/event.model';
import User from '../../api/user/user.model';

var helper = require('sendgrid').mail;

/**
 * Sends e-mail to object owner on changes
 */
export function sendNewEventEmail() {
  var subject = 'Someone created clan event at www.euroclans.eu';

  return compose()
    .use(function meetsRequirements(req, res, next) {
      Clan.findById(req.body.leftClan).exec()
        .then(clan => {
          if (clan) {
            var content = new helper.Content('text/plain', `
              Someone created event ${req.body.name}, where your clan ${clan.name} takes part.
              Check details at http://www.euroclans.eu.

              Your www.euroclans.eu team.
            `);
            clan.members.forEach(member => {
              User.findById(member).exec()
                .then(user => {
                  if (user) {
                    sendOneEmail(user.email, subject, content);
                  }
                });
            });
            return next();
          } else {
            return next();
          }
        });
    })
    .use(function meetsRequirements(req, res, next) {
      Clan.findById(req.body.rightClan).exec()
        .then(clan => {
          if (clan) {
            var content = new helper.Content('text/plain', `
              Someone created event ${req.body.name}, where your clan ${clan.name} takes part.
              Check details at http://www.euroclans.eu.

              Your www.euroclans.eu team.
            `);
            clan.members.forEach(member => {
              User.findById(member).exec()
                .then(user => {
                  if (user) {
                    sendOneEmail(user.email, subject, content);
                  }
                });
            });
            return next();
          } else {
            return next();
          }
        });
    });
}

/**
 * Sends e-mail to object owner on changes
 */
export function sendJoinRequestEmail() {
  return compose()
    .use(function meetsRequirements(req, res, next) {
      Clan.findById(req.body.clan).exec()
        .then(clan => {
          if (clan) {
            User.findById(clan.owner).exec()
              .then(user => {
                var content = new helper.Content('text/plain', `
                  Join request to your clan ${clan.name} have been posted. You can check it on your clan page -
                  http://www.euroclans.eu/clans/${clan._id}.

                  Your www.euroclans.eu team.
                `);
                sendOneEmail(user.email, 'Someone want to join your clan at www.euroclans.eu', content);
                return next();
              });
          } else {
            return next();
          }
        });
    });
}

/**
 * Sends e-mail to object owner on changes
 */
export function sendClanEmail() {
  return compose()
    .use(function meetsRequirements(req, res, next) {
      // Chat message added
      if (req.body[0].path === '/chat/0') {
        Clan.findById(req.params.id).exec()
          .then(clan => {
            User.findById(clan.owner).exec()
              .then(user => {
                if (user) {
                  let value = req.body[0].value;
                  var content = new helper.Content('text/plain', `
                    Message 
                    ${value.message}
                    is posted to your clan ${clan.name}. You can check it on your clan page -
                    http://www.euroclans.eu/clans/${clan._id}.

                    Your www.euroclans.eu team.
                  `);
                  sendOneEmail(user.email, 'Message posted for your clan at www.euroclans.eu', content);
                }
                return next();
              });
          });
      } else {
        return next();
      }
    });
}

/**
 * Sends chat e-mail messages to event users when event is patched.
 */
export function sendEventPatchEmail() {
  return compose()
    .use(function meetsRequirements(req, res, next) {
      // Chat message added
      if (req.body[0].path === '/chat/0') {
        Event.findById(req.params.id).exec()
          .then(event => {
            let value = req.body[0].value;
            var content = new helper.Content('text/plain', `
              Someone sent message on event you take part.
              ${value.message}
              You can respond to it at -
              http://www.euroclans.eu/events/${event._id}.

              Your www.euroclans.eu team.
            `);
            event.leftTeam.forEach(member => {
              User.findById(member).exec()
                .then(user => {
                  if (user) {
                    sendOneEmail(user.email, 'Someone posted message on event at www.euroclans.eu', content);
                  }
                });
            });
            event.rightTeam.forEach(member => {
              User.findById(member).exec()
                .then(user => {
                  if (user) {
                    sendOneEmail(user.email, 'Someone posted message on event at www.euroclans.eu', content);
                  }
                });
            });
            return next();
          });
      } else {
        return next();
      }
    });
}

function sendOneEmail(toEmailstr, subject, content) {
  var fromEmail = new helper.Email('Euroclans.eu <noreplay@euroclans.eu>');
  var toEmail = new helper.Email(toEmailstr);
  var mail = new helper.Mail(fromEmail, subject, toEmail, content);

  var sg = require('sendgrid')(process.env.SENDGRID_API_KEY);
  var request = sg.emptyRequest({
    method: 'POST',
    path: '/v3/mail/send',
    body: mail.toJSON(),
  });

  sg.API(request, function(error, response) {
    console.log(response.statusCode);
    console.log(response.body);
    console.log(response.headers);
  });
}
