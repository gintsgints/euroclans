/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import Game from '../api/game/game.model';
import GamePlatform from '../api/game_platform/game_platform.model';
import Clan from '../api/clan/clan.model';
import Event from '../api/event/event.model';
import User from '../api/user/user.model';
import config from './environment/';

export default function seedDatabaseIfNeeded() {
  if (config.seedDB) {
    User.find({}).remove()
      .then(() => {
        var user1 = {
          provider: 'local',
          name: 'Test User',
          email: 'polis.gints@gmail.com',
          password: 'test',
          icon: 'http://www.freeiconspng.com/uploads/games-icon-png-4.png'
        };
        var user2 = {
          provider: 'local',
          role: 'admin',
          name: 'Admin',
          email: 'admin@example.com',
          password: 'admin'
        };
        User.create([user1, user2])
          .then(doc => {
            user1 = doc[0];
            user2 = doc[1];
            let xbox1 = {
              name: 'Xbox One',
              img: 'xbox-one.png'
            };
            let ps2 = {
              name: 'Play Station',
              img: 'ps4.png'
            };
            let pc = {
              name: 'PC',
              img: 'pc.png'
            };

            GamePlatform.find({}).remove()
              .then(() => {
                GamePlatform.create([xbox1, ps2, pc])
                  .then(() => {
                    console.log('finished populating game platforms');
                    let Bf1Conquest60 = {
                      name: 'Conquest 60 vs 60',
                    };
                    let Bf1Conquest30 = {
                      name: 'Conquest 30 vs 30',
                    };
                    let Bf1Conquest25 = {
                      name: 'Conquest 25 vs 25',
                    };
                    let Bf1Conquest15 = {
                      name: 'Conquest 15 vs 15',
                    };
                    let Bf1Domination12 = {
                      name: 'Domination 12 vs 12',
                    };
                    let Bf1Domination10 = {
                      name: 'Domination 10 vs 10',
                    };
                    let Bf1Domination8 = {
                      name: 'Domination 8 vs 8',
                    };
                    let Bf1Domination5 = {
                      name: 'Domination 5 vs 5',
                    };
                    let CsgoHostageRescue = {
                      name: 'Hostage Rescue',
                    };
                    let CsgoBombDefusal = {
                      name: 'Bomb Defusal',
                    };

                    let bf1 = {
                      name: 'Battlefield 1',
                      gameTypes: [Bf1Conquest60, Bf1Conquest30, Bf1Conquest25, Bf1Conquest15, Bf1Domination12, Bf1Domination10, Bf1Domination8, Bf1Domination5],
                      gamePlatforms: [xbox1, ps2, pc],
                      img: 'bf1.jpg',
                      active: true
                    };
                    let csgo = {
                      name: 'Conter Strike GO',
                      gameTypes: [CsgoHostageRescue, CsgoBombDefusal],
                      gamePlatforms: [pc],
                      img: 'cs_go.png',
                      active: true
                    };

                    Game.find({}).remove()
                      .then(() => {
                        Game.create([bf1, csgo])
                          .then(() => console.log('finished populating games'))
                          .catch(err => console.log('error populating games', err));
                      });

                    let clan1 = {
                      name: 'Test Clan 1',
                      owner: user1._id,
                      tag: 'TG',
                      info: 'Testing Clan for development needs. This holds long text to analyze how clan object works.',
                      homepage: 'http://www.euroclans.eu',
                      img: 'http://img05.deviantart.net/1357/i/2015/019/9/6/myo_clan_logo_by_littleboyblack-d5v4rot.jpg',
                      game: bf1,
                      gameplatform: xbox1,
                      joinpassword: 'password',
                      active: Boolean,
                      members: [user1._id, user2._id],
                    };
                    let clan2 = {
                      name: 'Test Clan 2',
                      owner: user1._id,
                      tag: 'TCL2',
                      info: 'Testing Clan2 for development needs. This holds long text to analyze how clan object works.',
                      homepage: 'https://www.google.lv/',
                      img: 'http://cfw.sarna.net/wiki/images/d/d7/ClanWolverine.jpg',
                      game: bf1,
                      gameplatform: pc,
                      joinpassword: 'password',
                      active: Boolean,
                      members: [user2._id],
                    };

                    Clan.find({}).remove()
                      .then(() => {
                        Clan.create([clan1, clan2])
                          .then(cldoc => {
                            clan1 = cldoc[0];
                            clan2 = cldoc[1];
                            Event.find({}).remove()
                              .then(() => {
                                let event0 = {
                                  name: 'Custom Event',
                                  owner: user2._id,
                                  game: bf1,
                                  gametype: Bf1Domination10,
                                  gameplatform: xbox1
                                };
                                let event1 = {
                                  name: 'Battle in past',
                                  owner: user1._id,
                                  datetime: new Date() - 1,
                                  game: bf1,
                                  gametype: Bf1Conquest25,
                                  gameplatform: xbox1,
                                  leftClan: clan1,
                                  rightClan: clan2
                                };
                                let event2 = {
                                  name: 'Planed Clan Battle',
                                  owner: user1._id,
                                  datetime: new Date() + 1,
                                  game: bf1,
                                  gametype: Bf1Conquest25,
                                  gameplatform: xbox1,
                                  leftClan: clan1,
                                  rightClan: clan2,
                                  chat: [
                                    { message: 'First message', user: user1 },
                                    { message: 'Ansfer to First message', user: user2 },
                                    { message: 'Second message', user: user1 },
                                  ]
                                };
                                let event3 = {
                                  name: 'Custom Event Of Counter strike',
                                  owner: user1._id,
                                  datetime: new Date() + 1,
                                  game: csgo,
                                  gametype: CsgoHostageRescue,
                                  gameplatform: pc,
                                  leftClan: clan1,
                                  rightClan: clan2
                                };
                                Event.create([event0, event1, event2, event3])
                                  .then(() => console.log('finished populating events'))
                                  .catch(err => console.log('error populating events', err));
                              });
                            console.log('finished populating clans');
                          })
                          .catch(err => console.log('error populating clans', err));
                      });
                  })
                  .catch(err => console.log('error populating game platforms', err));
              });
          })
          .catch(err => console.log('error populating users', err));
      });
  }
}
