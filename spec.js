'use strict';
/*eslint-env node*/
var testsContext;

require('babel-polyfill');
require('angular');
require('angular-mocks');
require('./client/components/ui-router/ui-router.mock');
require('./client/components/socket/socket.mock');
require('./client/components/auth/auth.mock');
require('./client/components/calendar-config/calendar-config.mock');
require('./client/components/auth/user.service.mock');

testsContext = require.context('./client', true, /\.spec\.js$/);
testsContext.keys().forEach(testsContext);
